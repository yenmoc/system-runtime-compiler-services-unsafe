# System Runtime Compiler Services Unsafe

## What
	-System.Runtime.CompilerServices.Unsafe version 4.7.0

## Requirements
[![Unity 2018.3+](https://img.shields.io/badge/unity-2018.3+-brightgreen.svg?style=flat&logo=unity&cacheSeconds=2592000)](https://unity3d.com/get-unity/download/archive)
[![.NET 2.0 Scripting Runtime](https://img.shields.io/badge/.NET-2.0-blueviolet.svg?style=flat&cacheSeconds=2592000)](https://docs.unity3d.com/2019.1/Documentation/Manual/ScriptingRuntimeUpgrade.html)

## Installation

```bash
"com.yenmoc.system-runtime-compiler-services-unsafe":"https://gitlab.com/yenmoc/system-runtime-compiler-services-unsafe"
or
npm publish --registry http://localhost:4873
```

